namespace ERP.Dtos
{
    public class StyrmandsqueryDto
    {
        public MemberReadDto member {get; set;}
        

        public StyrmandsqueryDto (MemberReadDto memberdto)
        {
                this.member=memberdto;
           
        }
        public override bool Equals (object obj) 
        {
            var styrmand = obj as StyrmandsqueryDto;
            if (styrmand.member.MemberId == this.member.MemberId)
            {
                return true;
            }
            return false;
        }
        
    }
}