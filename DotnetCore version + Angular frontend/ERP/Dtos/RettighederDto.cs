using ERP.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ERP.Dtos
{
    public class RettighederDto
    {
        [Key]
        public int RetId { get; set; }

        public string RNavn { get; set; }
        public List<RettighedMemberDto> RettighedMembers { get; set; }

        // public List<RettighedNameDto> RettighedInstuctorNames { get; set; }


    }
}