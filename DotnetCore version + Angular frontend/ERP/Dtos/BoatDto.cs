using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ERP.Dtos
{
    public class BoatDto
    {
        [Key]
        public int BoatId { get; set; }
        public int Antal { get; set; }
        public string Bnavn { get; set; }
        public string Cox { get; set; }
        public string Inrig { get; set; }
        public string Outtype { get; set; }
        public string Idrift { get; set; }
        public int Orden { get; set; }
        public int Klub { get; set; }
        public string Kommentar { get; set; }
        public List<TurDto> Ture { get; set; }

    }
}