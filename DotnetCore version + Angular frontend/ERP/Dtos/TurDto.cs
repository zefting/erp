using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Dtos
{
    public class TurDto
    {
        [Key]
        public int? TripId { get; set; }
        public List<TurmedlemDto> Turmedlems { get; set; }
        public int BoatId { get; set; }
        public BoatDto Boat { get; set; }
        public bool Ltur { get; set; }
        public int DestiId { get; set; }
        public DestiDto Desti { get; set; }
        public string ForventetHjemkomst { get; set; }
        public string Hjemkomst { get; set; }
        public string Afgang { get; set; }


        public int? StyrmandId { get; set; }
        public MemberReadDto Styrmand { get; set; }


        public int KM { get; set; }
        public bool Afsluttet { get; set; }

    }
}
