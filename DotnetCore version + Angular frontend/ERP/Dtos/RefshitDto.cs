
namespace ERP.Dtos
{
    public class RefshitDto
    {
        public MemberReadDto member { get; set; }
        public RettighederDto rettigheder { get; set; }
        public RettighedMemberDto rettighedMember { get; set; }
        public MemberReadDto instructor { get; set; }
        public RefshitDto(MemberReadDto memberdto, RettighederDto rettighederdto, RettighedMemberDto rettighedMemberdto, MemberReadDto instructor)
        {
            this.member = memberdto;
            this.rettigheder = rettighederdto;
            this.rettighedMember = rettighedMemberdto;
            this.instructor = instructor;

        }

    }
}