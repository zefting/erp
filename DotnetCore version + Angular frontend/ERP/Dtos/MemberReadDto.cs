using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using ERP.Dtos;
namespace ERP.Dtos
{
    public class MemberReadDto
    {
        [Key]
        public int MemberId { get; set; }
        public List<TurmedlemDto> Turmedlems { get; set; }
        public int Medlemsnummer { get; set; }

        public string Fornavn { get; set; }
        public string Mellemnavn { get; set; }

        public string Efternavn { get; set; }
        public string Adresse { get; set; }
        public string Telefon { get; set; }
        public string Postnummer { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string Bdato { get; set; }
        public List<RettighedMemberDto> RettighedMembers { get; set; }
        public List<RettighedMemberDto> Styrmand { get; set; }
        //   public List<RettighedNameDto> RettighedInstuctorNames { get; set; } 

        public List<TurDto> Ture { get; set; }

    }
}