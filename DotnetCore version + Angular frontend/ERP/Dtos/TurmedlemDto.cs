using ERP.Models;

//using Boat.Dtos;
namespace ERP.Dtos
{
    public class TurmedlemDto
    {
        public int MemberId { get; set; }
        public MemberReadDto Member { get; set; }
        public int TripId { get; set; }
        public TurDto Trip { get; set; }

    }
}