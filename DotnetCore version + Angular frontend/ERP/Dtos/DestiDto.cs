
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ERP.Models;
namespace ERP.Dtos
{
    public class DestiDto
    {
        [Key]
        public int DestiId { get; set; }
        public string Dnavn { get; set; }
        public List<TurDto> Ture { get; set; }
        public List<DestiDto> Destinations { get; set; }

    }

}