using ERP.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ERP.Dtos
{
    public class RettighedMemberDto
    {
        [Key]
        public int? RetId { get; set; }
        public RettighederDto Rettighed { get; set; }



        public int MemberId { get; set; }
        public MemberReadDto Member { get; set; }



        public string Dato { get; set; }



        public MemberReadDto Instuctor { get; set; }

        public int? InstuctorId { get; set; }

    }
}