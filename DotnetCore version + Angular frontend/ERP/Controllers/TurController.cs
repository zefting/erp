using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ERP.Data;
using ERP.Models;
using AutoMapper;
using ERP.Dtos;

namespace ERP.Controllers
{
    [Route("api/turs/")]
    [ApiController]
    public class TursController : ControllerBase
    {
        private readonly IERPRepo _repository;

        private readonly IMapper _mapper;

        public TursController(IERPRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("getAllTurs")]
        public ActionResult<List<Tur>> GetAllTurs()
        {
            var turItems = _repository.GetAllTurs();
            if (turItems != null)
            {
                return Ok(turItems);
            }
            return NotFound();
        }
        [Route("GetCurrentTurs")]
        public ActionResult<List<Tur>> GetCurrentTurs()
        {
            var turItems = _repository.GetCurrentTurs();
            if (turItems != null)
            {
                return Ok(turItems);
            }
            return NotFound();
        }
        [HttpGet("getTurById/{id}")]
        public ActionResult<Tur> GetTurById(int id)
        {
            var turItem = _repository.GetTurById(id);
            if (turItem != null)
            {
                return Ok(turItem);
            }
            return NotFound();
        }

        [HttpPost]
        public ActionResult<Tur> Gemtur(Tur tur)
        {
            var turModel = _mapper.Map<TurDto>(tur);

            _repository.GemTur(turModel);
            _repository.SaveChanges();
            var result = _mapper.Map<Tur>(turModel);
            var response = _repository.GetTurById(result.TripId.Value);

            return Ok(response);
        }

        [HttpPut("updateTur/{id}")]
        public ActionResult UpdateTur(int id, Tur tur)
        {
            var turmodelFromRepo = _repository.GetTurDtoById(id);
            if (turmodelFromRepo == null)
            {
                return NotFound();
            }
            _mapper.Map(tur, turmodelFromRepo);

            _repository.UpdateTur(turmodelFromRepo);

            _repository.SaveChanges();

            return NoContent();

        }
    }
}
