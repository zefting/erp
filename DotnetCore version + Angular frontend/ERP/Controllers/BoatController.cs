using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ERP.Data;
using ERP.Dtos;
using AutoMapper;

namespace ERP.Controllers
{

    [Route("api/boats")]
    [ApiController]
    public class BoatsController : ControllerBase
    {
        private readonly IERPRepo _repository;
        private readonly IMapper _mapper;

        public BoatsController(IERPRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        [HttpGet]
        public ActionResult<IEnumerable<BoatDto>> GetAllBoats()
        {
            var boatItems = _repository.GetAllBoats();
            if (boatItems != null)
            {
                return Ok(boatItems);
            }
            return NotFound();
        }
    }
}