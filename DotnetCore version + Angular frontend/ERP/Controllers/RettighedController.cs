using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ERP.Data;
using ERP.Dtos;
using ERP.Models;
using AutoMapper;
using System.Linq;

namespace ERP.Controllers
{

    [Route("api/Rettigheder")]
    [ApiController]
    public class RettighederController : ControllerBase
    {

        private readonly IERPRepo _repository;
        private readonly IMapper _mapper;

        public RettighederController(IERPRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("GetRettighedNameById")]
        public ActionResult<IQueryable<MemberRightsView>> GetRettighedNameById(int MemberId)
        {
            var AllRettigheder = _repository.GetRettighedNameById(MemberId);

            if (AllRettigheder != null)
            {
                List<MemberRightsView> memberrights = new List<MemberRightsView>();
                foreach (var medlemmedrettigheder in AllRettigheder)
                {
                    var RettighedMember = new Refshit(_mapper.Map<Member>(medlemmedrettigheder.member),
                                                      _mapper.Map<Rettigheder>(medlemmedrettigheder.rettigheder),
                                                      _mapper.Map<RettighedMember>(medlemmedrettigheder.rettighedMember),
                                                      _mapper.Map<Member>(medlemmedrettigheder.instructor)
                                                      );
                    var view = new MemberRightsView(RettighedMember.member.Fornavn,
                                                     RettighedMember.member.Mellemnavn,
                                                     RettighedMember.member.Efternavn,
                                                     RettighedMember.member.MemberId,
                                                     RettighedMember.instructor.Fornavn,
                                                     RettighedMember.instructor.Mellemnavn,
                                                     RettighedMember.instructor.Efternavn,
                                                     RettighedMember.instructor.MemberId,
                                                     new List<Rettigheder>() { RettighedMember.rettigheder },
                                                     RettighedMember.member.Email,
                                                     RettighedMember.rettigheder.RetId,
                                                     RettighedMember.member.Telefon,
                                                     RettighedMember.member.Adresse,
                                                     RettighedMember.member.Postnummer,
                                                     RettighedMember.member.City
                                                     );


                    memberrights.Add(view);

                }



                return Ok(memberrights);


            }
            return NotFound();
        }


        [HttpGet]
        [Route("GetAllRettighedName")]
        public ActionResult<List<MemberRightsView>> GetAllRettighedname()
        {
            var AllRettigheder = _repository.GetAllRettighedname();

            if (AllRettigheder != null)
            {
                List<MemberRightsView> memberrights = new List<MemberRightsView>();
                foreach (var medlemmedrettigheder in AllRettigheder)
                {
                    var RettighedMember = new Refshit(_mapper.Map<Member>(medlemmedrettigheder.member),
                                                      _mapper.Map<Rettigheder>(medlemmedrettigheder.rettigheder),
                                                      _mapper.Map<RettighedMember>(medlemmedrettigheder.rettighedMember),
                                                      _mapper.Map<Member>(medlemmedrettigheder.instructor)
                                                      );
                    var view = new MemberRightsView(RettighedMember.member.Fornavn,
                                                     RettighedMember.member.Mellemnavn,
                                                     RettighedMember.member.Efternavn,
                                                     RettighedMember.member.MemberId,
                                                     RettighedMember.instructor.Fornavn,
                                                     RettighedMember.instructor.Mellemnavn,
                                                     RettighedMember.instructor.Efternavn,
                                                     RettighedMember.instructor.MemberId,
                                                     new List<Rettigheder>() { RettighedMember.rettigheder },
                                                     RettighedMember.member.Email,
                                                     RettighedMember.rettigheder.RetId,
                                                     RettighedMember.member.Telefon,
                                                     RettighedMember.member.Adresse,
                                                     RettighedMember.member.Postnummer,
                                                     RettighedMember.member.City
                                                     );


                    memberrights.Add(view);

                }



                return Ok(memberrights);


            }
            return NotFound();
        }

        [HttpGet]
        [Route("GetAllRettigheder")]
        public ActionResult<IEnumerable<Rettigheder>> GetAllRettigheds()
        {
            var RettighedItems = _repository.GetAllRettigheds();
            if (RettighedItems != null)
            {
                return Ok(RettighedItems);
            }
            return NotFound();
        }
        [HttpPost]

        public ActionResult<RettighedMember> GemRettighedName(RettighedMember rettighedMember)
        {
            var rettighedNameModel = _mapper.Map<RettighedMemberDto>(rettighedMember);

            _repository.GemRettighedName(rettighedNameModel);
            _repository.SaveChanges();
            var result = _mapper.Map<RettighedMember>(rettighedNameModel);
            // var response = _repository.GetTurById(result.TripId.Value);

            return Ok();
        }

    }
}