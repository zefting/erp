using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ERP.Data;
using ERP.Models;
using AutoMapper;
using ERP.Dtos;
namespace ERP.Controllers
{
    [Route("api/distis/")]
    [ApiController]

    public class DistiController : ControllerBase
    {
        private readonly IERPRepo _repository;
        private readonly IMapper _mapper;
        public DistiController(IERPRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        [HttpGet]
        public ActionResult<IEnumerable<Destis>> GetAllDisti()
        {
            var destiItems = _repository.GetAllDisti();
            if (destiItems != null)
            {
                return Ok(destiItems);
            }
            return NotFound();
        }

    }
}

