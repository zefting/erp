using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ERP.Data;
using ERP.Dtos;
using ERP.Models;
using AutoMapper;

namespace ERP.Controllers
{
    [Route("api/members")]
    [ApiController]
    public class NamesController : ControllerBase
    {
        private readonly IERPRepo _repository;
        private readonly IMapper _mapper;

        public NamesController(IERPRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        [HttpGet]
        public ActionResult<IEnumerable<MemberReadDto>> GetAllNavns()
        {
            var navnItems = _repository.GetAllNavns();
            if (navnItems != null)
            {
                return Ok(navnItems);
            }
            return NotFound();
        }
        [HttpGet]
        [Route("GetStyrmand")]
        public ActionResult<List<styrmandquery>> GetStyrmand()
        {
            var styrmandItem = _repository.GetStyrmand();
            if (styrmandItem != null)
            {
                var styrList = new List<styrmandquery>();

                foreach (var Styrret in styrmandItem)
                {
                    var styrmandview = new styrmandquery(Styrret.member.MemberId,
                                                        Styrret.member.Fornavn,
                                                        Styrret.member.Mellemnavn,
                                                        Styrret.member.Efternavn,
                                                        Styrret.member.Medlemsnummer
                                                        );
                    styrList.Add(styrmandview);
                }

                return Ok(styrList);
            }
            return NotFound();
        }


        [HttpGet("getMemberById/{id}")]
        public ActionResult<Tur> getMemberById(int id)
        {
            var MemberIdItem = _repository.getMemberById(id);
            if (MemberIdItem != null)
            {
                return Ok(MemberIdItem);
            }
            return NotFound();
        }


        [HttpPut("updatemember/{id}")]
        public ActionResult UpdateMember(int id, Member member)
        {
            var MembermodelFromRepo = _repository.getMemberById(id);
            if (MembermodelFromRepo == null)
            {
                return NotFound();
            }
            _mapper.Map(member, MembermodelFromRepo);

            _repository.UpdateMember(MembermodelFromRepo);

            _repository.SaveChanges();

            return NoContent();

        }
        [HttpPost]
        public ActionResult<Member> Gemtur(Member member)
        {
            var memberModel = _mapper.Map<MemberReadDto>(member);

            _repository.GemMember(memberModel);
            _repository.SaveChanges();
            var result = _mapper.Map<Member>(memberModel);
            var response = _repository.getMemberById(result.MemberId);

            return Ok(response);
        }
    }
}
