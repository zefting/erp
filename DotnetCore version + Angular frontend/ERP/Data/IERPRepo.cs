using System.Collections.Generic;
using ERP.Dtos;
using ERP.Models;
using System.Linq;




namespace ERP.Data
{
    public interface IERPRepo
    {
        bool SaveChanges();
        IEnumerable<MemberReadDto> GetAllNavns();
        IEnumerable<TurDto> GetAllTurDtos();
        TurDto GetTurDtoById(int id);
        Tur GetTurById(int turid);
        MemberReadDto getMemberById(int turid);

        List<Tur> GetCurrentTurs();
        List<Tur> GetAllTurs();
        List<StyrmandsqueryDto> GetStyrmand();
        IEnumerable<RettighederDto> GetAllRettigheds();
        List<RefshitDto> GetAllRettighedname();
        IQueryable<RefshitDto> GetRettighedNameById(int memberId);
        void GemRettighedName(RettighedMemberDto rettighedNameDto);
        void GemTur(TurDto turDto);
        void UpdateTur(TurDto turDto);
        void UpdateMember(MemberReadDto nameReadDto);
        void GemMember(MemberReadDto memberReadDto);
        IEnumerable<BoatDto> GetAllBoats();

        IEnumerable<DestiDto> GetAllDisti();
    }

}
