using Microsoft.EntityFrameworkCore;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections;
using System.Collections.Generic;
using ERP.Dtos;
using ERP.Models;

namespace ERP.Data
{
    public class ErpContext : DbContext
    {
        public ErpContext(DbContextOptions<ErpContext> opt) : base(opt)
        {

        }
        public DbSet<MemberReadDto> Members { get; set; }
        public DbSet<TurDto> Turs { get; set; }
        public DbSet<TurmedlemDto> Turmedlems { get; set; }
        public DbSet<BoatDto> Boats { get; set; }
        public DbSet<DestiDto> Destis { get; set; }

        public DbSet<RettighederDto> Rettigheders { get; set; }
        public DbSet<RettighedMemberDto> RettighedMembers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)


        {
            modelBuilder.Entity<TurmedlemDto>()
                .HasKey(bc => new { bc.TripId, bc.MemberId, /*bc.BoatId, bc.DestiId */});

            modelBuilder.Entity<TurmedlemDto>()
                .HasOne(bc => bc.Trip)
                .WithMany(b => b.Turmedlems)
                .HasForeignKey(bc => bc.TripId);

            modelBuilder.Entity<TurmedlemDto>()
                .HasOne(bc => bc.Member)
                .WithMany(c => c.Turmedlems)
                .HasForeignKey(bc => bc.MemberId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<TurDto>(t =>
                {   // one to many mellem både og ture
            t.Property(e => e.TripId).ValueGeneratedOnAdd();
                    t.HasOne(tur => tur.Boat)
                    .WithMany(boat => boat.Ture)
                    .HasForeignKey(tur => tur.BoatId)
                    .OnDelete(DeleteBehavior.NoAction);
            // one to many mellem destinationer og ture
            t.HasOne(tur => tur.Desti)
                    .WithMany(desti => desti.Ture)
                    .HasForeignKey(tur => tur.DestiId)
                    .OnDelete(DeleteBehavior.NoAction);
                    t.HasOne(tur => tur.Styrmand)
                    .WithMany(styrmand => styrmand.Ture)
                    .HasForeignKey(tur => tur.StyrmandId);
                });


            modelBuilder.Entity<MemberReadDto>(t =>
            {
                t.Property(e => e.MemberId).ValueGeneratedOnAdd();
            });
            modelBuilder.Entity<RettighederDto>(t =>
            {
                t.Property(e => e.RetId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<BoatDto>(t =>
            {
                t.Property(e => e.BoatId).ValueGeneratedOnAdd();

            });
            modelBuilder.Entity<DestiDto>(t =>
            {
                t.Property(e => e.DestiId).ValueGeneratedOnAdd();
        //t.HasOne(tur => tur.Desti)
        //.WithMany(desti => desti.Ture)
        //.HasForeignKey(tur => tur.DestiId)
        //.OnDelete(DeleteBehavior.NoAction);
    });


            modelBuilder.Entity<RettighedMemberDto>(t =>
            {
                t.HasKey(bc => new { bc.RetId, bc.InstuctorId, bc.MemberId });
        //modelBuilder.Entity<RettighedMemberDto>()
        //.HasOne(bc => bc.Rettighed)
        //.WithMany(bc => bc.RettighedInstuctorNames)
        //.HasForeignKey(bc => bc.RetId);

        modelBuilder.Entity<RettighedMemberDto>()
       .HasOne(bc => bc.Rettighed)
       .WithMany(bc => bc.RettighedMembers)
       .HasForeignKey(bc => bc.RetId);

                modelBuilder.Entity<RettighedMemberDto>()
            .HasOne(bc => bc.Member)
            .WithMany(b => b.RettighedMembers)
            .HasForeignKey(bc => bc.MemberId)
            .OnDelete(DeleteBehavior.NoAction);
            }

            // den laver et felt der hedder Names1 med fk til name tabellen
            // modelBuilder.Entity<RettighedNameDto>() 
            //   .HasOne(bc => bc.Instuctor)
            // .WithMany(c => c.RettighedInstuctorNames)
            //  .HasForeignKey(bc => bc.MemberId)
            // .HasConstraintName("fk_Instuctor")
            //.OnDelete(DeleteBehavior.NoAction);    

            );
        }
    }
}