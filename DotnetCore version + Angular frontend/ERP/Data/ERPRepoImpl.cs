using System.Collections.Generic;
using System.Linq;
using System;
using ERP.Dtos;
using ERP.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
namespace ERP.Data
{
    public class ERPRepoImpl : IERPRepo
    {
        List<int?> k1 = new List<int?>(){
            1, //Styrmandens ansvar, rettigheder og pligter
            2,//Kommandoer og manøvrer (herunder tillægning ved broer i havnen og ved badebroer langs kysten)
            3,   //Faresituationer; teoretisk kendskab til forholdsregler ved: a) påsejling af sten eller pæle (f.eks. ved Åkrogen, ved Skæring Hede, ved Vosnæsgård) b) kollision c) bordfyldning d) kæntring
            4, //Hårdtvejrstur a) styreteknik b) indøvning af hurtigt og sikkert skift c) interferensbølger ved den Permanente og ved moler.
            5,  // Søvejsregler: bl.a. vigeregler, ansvar og udkig.
            6,  // Farvandsafmærkninger efter IALA System A, f.eks. sideafmærkning ved Studstrup og special afmærkning ('gule bøjer') mellem Egå og Studstrup, og dykkerflag. 
            7,   // Bådens fagbetegnelser 
            8,  // Værktøj: Kendskab til skruer og skruetrækker, specialværktøj, lim og lak. Opmærksomhed og vedligehold: Udskiftning eller fastspænding af løse skruer, samt udskiftning af hjul og skinner. Skader: Procedure i tilfælde af skader, opdaget eller forårsaget. Sk
            9, // Landgang på åben kyst, hvor båden bæres i land og understøttes.
            10   // Gennemsejling af K-retsområdet til Studstrup, samt gennemgang af søkort over K1-retsområdet. 
            };








        List<int?> k2 = new List<int?>()
            {
                11,12,13,14,15,20,23,25
            };

        List<int?> k3 = new List<int?>()
            {
                16,17,18,24,26
            };
        List<int?> Lret = new List<int?>()
            {27};

        private readonly ErpContext _context;
        private readonly IMapper _mapper;
        public ERPRepoImpl(ErpContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public void GemTur(TurDto turDto)
        {
            if (turDto == null)
            {
                throw new ArgumentNullException(nameof(turDto));
            }
            _context.Turs.Add(turDto);
        }
        public void UpdateTur(TurDto turDto)
        {
            if (turDto == null)
            {
                throw new ArgumentNullException(nameof(turDto));
            }
        }
        public void UpdateMember(MemberReadDto nameReadDto)
        {
            if (nameReadDto == null)
            {
                throw new ArgumentNullException(nameof(nameReadDto));
            }
        }
        public void GemMember(MemberReadDto memberReadDto)
        {
            if (memberReadDto == null)
            {
                throw new ArgumentNullException(nameof(memberReadDto));
            }
            _context.Members.Add(memberReadDto);
        }

        public IEnumerable<BoatDto> GetAllBoats()
        {
            return _context.Boats.ToList();
        }
        public IEnumerable<DestiDto> GetAllDisti()
        {
            return _context.Destis.ToList();
        }
        public IEnumerable<MemberReadDto> GetAllNavns()
        {
            return _context.Members.ToList();
        }

        public MemberReadDto getMemberById(int id)
        {
            return _context.Members.FirstOrDefault(p => p.MemberId == id);
        }



        public IEnumerable<TurDto> GetAllTurDtos()
        {
            return _context.Turs.ToList();
        }


        public TurDto GetTurDtoById(int id)
        {
            return _context.Turs.FirstOrDefault(p => p.TripId == id);
        }

        public Tur GetTurById(int turid)
        {
            var tur = _context.Turs
            .Where(tur => tur.TripId == turid)
            .Include(tur => tur.Boat)
            .Include(tur => tur.Desti)
            .Include(tur => tur.Turmedlems)
            .ThenInclude(turmedlem => turmedlem.Member)
            .FirstOrDefault();
            if (tur != null)
            {
                var styrmandsID = tur.StyrmandId;
                var styrmand = _context.Members.Where(member => member.MemberId == styrmandsID).FirstOrDefault();
                tur.Styrmand = styrmand;
            }
            return _mapper.Map<Tur>(tur);
        }

        public List<Tur> GetAllTurs()
        {
            var turWithMedlemmerList = new List<Tur>();
            var turs = _context.Turs
            .Include(tur => tur.Boat)
            .Include(tur => tur.Desti)
            .Include(p => p.Turmedlems)
            .ThenInclude(p => p.Member)
            .ToList();

            foreach (var tur in turs)
            {
                var styrmandsID = tur.StyrmandId;
                var styrmand = _context.Members.Where(member => member.MemberId == styrmandsID).FirstOrDefault();
                tur.Styrmand = styrmand;
            }

            return turs.Select(turDto => _mapper.Map<Tur>(turDto)).ToList();


        }
        //tæller member ture
        public int GetTurKmForMember(int memberId)
        {
            var turmedlems = _context.Turmedlems
            .Include(tm => tm.Trip)
            .Include(tm => tm.Member)
            .Where(tm => tm.MemberId == memberId)
            .ToList();
            int kmsum = 0;
            foreach (var tm in turmedlems)
            {
                kmsum += tm.Trip.KM;

            }

            return kmsum;

        }
        //tæller alle bådens km
        public BoatKmCountView GetTurKmForBoat(int boatId)
        {
            var trips = _context.Turs
            .Include(tm => tm.Boat)
            .Where(tm => tm.BoatId == boatId)
            .ToList();
            int kmsum = 0;
            var tripscount = trips.Count();

            foreach (var tm in trips)
            {
                kmsum += tm.KM;
            }

            return new BoatKmCountView(trips.FirstOrDefault()?.Boat?.Bnavn, kmsum, trips.FirstOrDefault()?.Boat?.BoatId, tripscount);

        }
        //tjekker om styrmanden har en rettighed
        public List<StyrmandsqueryDto> GetStyrmand()
        {
            var Styrmands =
            (from member in _context.Members
             join rettighedMember in _context.RettighedMembers on member.MemberId equals rettighedMember.MemberId

             where k1.Contains(rettighedMember.RetId)
                || k2.Contains(rettighedMember.RetId)
                || k3.Contains(rettighedMember.RetId)
                || Lret.Contains(rettighedMember.RetId)
             select new StyrmandsqueryDto(member)

            ).Distinct().ToList();






            return Styrmands;

        }
        public List<RettighederDto> RettighederForMedlem(int? memberid)
        {
            var rettigheder =
            (from member in _context.Members
             join rettighedMember in _context.RettighedMembers.Include(ret => ret.Rettighed) on member.MemberId equals rettighedMember.MemberId
             where member.MemberId == memberid
             select rettighedMember.Rettighed

            ).ToList();

            return rettigheder;

        }
        public IEnumerable<RettighederDto> GetAllRettigheds()
        {
            return _context.Rettigheders.ToList();
        }

        public List<RefshitDto> GetAllRettighedname()
        {
            var Rettighednames =
            (
                from rettighedMember in _context.RettighedMembers
                join member in _context.Members on rettighedMember.MemberId equals member.MemberId
                join rettigheder in _context.Rettigheders on rettighedMember.RetId equals rettigheder.RetId
                join instructor in _context.Members on rettighedMember.InstuctorId equals instructor.MemberId
                select new RefshitDto(member, rettigheder, rettighedMember, instructor)

            ).ToList();

            return Rettighednames;


        }
        public IQueryable<RefshitDto> GetRettighedNameById(int memberId)
        {
            var Rettighednames =
            (

                from rettighedMember in _context.RettighedMembers
                join member in _context.Members on rettighedMember.MemberId equals member.MemberId
                where member.MemberId == memberId
                join rettigheder in _context.Rettigheders on rettighedMember.RetId equals rettigheder.RetId
                join instructor in _context.Members on rettighedMember.InstuctorId equals instructor.MemberId

                select new RefshitDto(member, rettigheder, rettighedMember, instructor)

            );

            return Rettighednames;


        }

        public void GemRettighedName(RettighedMemberDto rettighedNameDto)
        {
            if (rettighedNameDto == null)
            {
                throw new ArgumentNullException(nameof(rettighedNameDto));
            }
            _context.RettighedMembers.Add(rettighedNameDto);
        }



        public List<Tur> GetCurrentTurs()
        {
            var turwhereTure = new List<Tur>();
            var getall = _context.Turs.
            Where(b => b.Afsluttet == false)
            .Include(tur => tur.Boat)
            .Include(p => p.Turmedlems)
            .ThenInclude(p => p.Member)
            .ToList();
            return getall.Select(turDto => _mapper.Map<Tur>(turDto)).ToList();
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }
        public List<Destis> DestiStatestik()
        {
            var destiwithTureList = new List<Destis>();
            var destiture = _context.Destis
            .Include(tur => tur.Destinations)
            .Include(tur => tur.Ture)
            .ToList();

            return destiture.Select(destiDto => _mapper.Map<Destis>(destiDto)).ToList();
        }
    }
}