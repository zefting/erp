
using System.Collections.Generic;



namespace ERP.Models
{
    public class TurWithMember
    {
        public Tur Tur { get; set; }
        public List<Member> Member { get; set; }
    }
}