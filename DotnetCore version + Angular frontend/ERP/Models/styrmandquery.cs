namespace ERP.Models
{
    public class styrmandquery
    {
        public int MemberId { get; set; }
        public string Fornavn { get; set; }
        public string Mellemnavn { get; set; }
        public string Efternavn { get; set; }
        public int Medlemnummer { get; set; }
        public styrmandquery(int memberId, string fornavn, string mellemnavn, string efternavn, int medlemsnummer)
        {
            this.MemberId = memberId;
            this.Fornavn = fornavn;
            this.Mellemnavn = mellemnavn;
            this.Efternavn = efternavn;
            this.Medlemnummer = medlemsnummer;
        }
    }
}