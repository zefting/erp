
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ERP.Models
{
    public class Member
    {
        [Key]
        public int MemberId { get; set; }

        public int Medlemsnummer { get; set; }

        public string Fornavn { get; set; }
        public string Mellemnavn { get; set; }
        public string Efternavn { get; set; }
        public string Adresse { get; set; }
        public string Telefon { get; set; }
        public string Postnummer { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string Bdato { get; set; }

        public List<RettighedMember> Rettighed { get; set; }
        public List<RettighedMember> GivenBy { get; set; }

    }
}