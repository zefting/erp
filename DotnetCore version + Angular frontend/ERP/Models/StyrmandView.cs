using System.Collections.Generic;
namespace ERP.Models
{
    public class StyrmandView
    {
        public int MemberId { get; set; }
        public string fornavn { get; set; }
        public string mellemnavn { get; set; }

        public string efternavn { get; set; }



        public StyrmandView(int MemberId, string fornavn, string mellemnavn, string efternavn)
        {
            this.MemberId = MemberId;
            this.fornavn = fornavn;
            this.mellemnavn = mellemnavn;
            this.efternavn = efternavn;

        }
    }
}
