namespace Opret.Models
{
    public class Setboat
    {
        public int Id { get; set; }
        public string MedlemsId { get; set; }
        public string BoatId { get; set; }
        public string Fordestination { get; set; }
        public int Km { get; set; }
        public string ForventetHjemkomst { get; set; }
    }

}