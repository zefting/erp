using System.ComponentModel.DataAnnotations;
namespace ERP.Models
{
    public class Boat
    {
        public int BoatId { get; set; }

        public int Antal { get; set; }

        public string Bnavn { get; set; }
        public int Cox { get; set; }

        public int Inrig { get; set; }


        public string Outtype { get; set; }

        public string Idrift { get; set; }
        public int Orden { get; set; }
        public int Klub { get; set; }
        public string Kommentar { get; set; }


    }
}