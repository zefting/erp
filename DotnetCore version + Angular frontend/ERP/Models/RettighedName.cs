using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Dtos;
namespace ERP.Models
{
    public class RettighedMember
    {
        [Key]
        public int? RetId { get; set; }
        public Rettigheder Rettighed { get; set; }



        public int MemberId { get; set; }
        public Member Member { get; set; }



        public string Dato { get; set; }



        public Member Instuctor { get; set; }

        public int? InstuctorId { get; set; }
    }

}