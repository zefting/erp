namespace ERP.Models
{
    public class BoatKmCountView
    {
        public string Bnavn { get; set; }
        public int KM { get; set; }
        public int? BoatId { get; set; }
        public int Turcount { get; set; }

        public BoatKmCountView(string bnavn, int km, int? boatId, int turcount)
        {
            this.Bnavn = bnavn;
            this.KM = km;
            this.BoatId = boatId;
            this.Turcount = turcount;
        }
    }

}