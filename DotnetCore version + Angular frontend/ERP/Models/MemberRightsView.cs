using System.Collections.Generic;

namespace ERP.Models
{
    public class MemberRightsView
    {
        public string fornavn { get; set; }
        public string mellem { get; set; }
        public string efter { get; set; }
        public int medlemid { get; set; }
        public string Instruktørfornavn { get; set; }
        public string Instruktørmellemnavn { get; set; }
        public string Instruktørefternavn { get; set; }
        public int instrktørid { get; set; }

        public List<Rettigheder> rettighedsdef { get; set; }

        public string email { get; set; }
        public int retId { get; set; }
        public string tele { get; set; }
        public string adresse { get; set; }
        public string postnummer { get; set; }
        public string city { get; set; }


        public MemberRightsView(string fornavn, string mellem, string efter, int medlemid, string Instruktørfornavn, string Instruktørmellemnavn,
                                string Instruktørefternavn, int instrktørid, List<Rettigheder> rettighedsdef, string email, int retId, string tele, string adresse,
                                string postnummer, string city)
        {
            this.fornavn = fornavn;
            this.mellem = mellem;
            this.efter = efter;
            this.medlemid = medlemid;
            this.Instruktørfornavn = Instruktørfornavn;
            this.Instruktørmellemnavn = Instruktørmellemnavn;
            this.Instruktørefternavn = Instruktørefternavn;
            this.instrktørid = instrktørid;
            this.rettighedsdef = rettighedsdef;
            this.email = email;
            this.retId = retId;
            this.tele = tele;
            this.adresse = adresse;
            this.postnummer = postnummer;
            this.city = city;
        }
    }
}