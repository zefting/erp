
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Dtos;
using System;


namespace ERP.Models
{
    public class Tur
    {
        public int? TripId { get; set; }
        public int BoatId { get; set; }
        public Boat Boat { get; set; }

        public string ForventetHjemkomst { get; set; }
        public string Afgang { get; set; }
        public List<Turmedlem> Turmedlems { get; set; }
        public bool Ltur { get; set; }
        public int KM { get; set; }
        public bool Afsluttet { get; set; }
        public string Hjemkomst { get; set; }
        // her skal der også en fk ind og et objekt som henter navnet ud - skal også tjekke om personen er styrmand ved at tjekke rettighedstabllen

        public int? StyrmandId { get; set; }
        public Member Styrmand { get; set; }

        public int DestiId { get; set; }

        public Destis Desti { get; set; }
        public DateTime oprettet { get; set; }
    }

}