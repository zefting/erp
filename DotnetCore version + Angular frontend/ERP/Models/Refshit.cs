namespace ERP.Models
{
    public class Refshit
    {
        public Member member { get; set; }
        public Rettigheder rettigheder { get; set; }
        public RettighedMember rettighedMember { get; set; }
        public Member instructor { get; set; }
        public Refshit(Member member, Rettigheder rettigheder, RettighedMember rettighedMember, Member instructor)
        {
            this.member = member;

            this.rettigheder = rettigheder;

            this.rettighedMember = rettighedMember;

            this.instructor = instructor;

        }
    }
}