
using System.Collections.Generic;

using ERP.Dtos;
using System;
namespace ERP.Models
{

    public class Destis
    {

        public int DestiId { get; set; }
        public string Dnavn { get; set; }
        public DestiDto Destination { get; set; }

    }
}