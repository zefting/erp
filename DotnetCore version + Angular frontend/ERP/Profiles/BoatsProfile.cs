using AutoMapper;
using ERP.Models;
using ERP.Dtos;

namespace ERP.Profiles
{
    public class BoatsProfile : Profile
    {
        public BoatsProfile()
        {
            CreateMap<Boat, BoatDto>();
            CreateMap<BoatDto, Boat>();
        }
    }
}