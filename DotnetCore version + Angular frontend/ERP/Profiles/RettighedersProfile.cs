using AutoMapper;
using ERP.Models;
using ERP.Dtos;

namespace ERP.Profiles
{
    public class RettighedersProfile : Profile
    {
        public RettighedersProfile()
        {   //sorce -> target
            CreateMap<Rettigheder, RettighederDto>();
            CreateMap<RettighederDto, Rettigheder>();
            CreateMap<RettighedMember, RettighedMemberDto>();
            CreateMap<RettighedMemberDto, RettighedMember>();

        }
    }
}