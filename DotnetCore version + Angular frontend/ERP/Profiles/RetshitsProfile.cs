using AutoMapper;
using ERP.Models;
using ERP.Dtos;
namespace ERP.Profiles

{
    public class RetshitsProfile : Profile
    {
        public RetshitsProfile()
        {   //sorce -> target
            CreateMap<Refshit, RefshitDto>();
            CreateMap<RefshitDto, Refshit>();

        }
    }
}