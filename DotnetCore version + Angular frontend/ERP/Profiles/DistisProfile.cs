using AutoMapper;
using ERP.Models;
using ERP.Dtos;

namespace ERP.Profiles
{
    public class DistisProfile : Profile
    {
        public DistisProfile()
        {
            CreateMap<Destis, DestiDto>();
            CreateMap<DestiDto, Destis>();
            CreateMap<Destis, Tur>();

        }
    }
}