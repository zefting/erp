using AutoMapper;
using ERP.Models;
using ERP.Dtos;

namespace ERP.Profiles
{
    public class TripsProfile : Profile
    {
        public TripsProfile()
        {   //sorce -> target
            CreateMap<Tur, TurDto>();
            CreateMap<TurDto, Tur>();
            CreateMap<Turmedlem, TurmedlemDto>();
            CreateMap<TurmedlemDto, Turmedlem>();
            CreateMap<MemberReadDto, Member>();
            CreateMap<Member, MemberReadDto>();
        }
    }
}