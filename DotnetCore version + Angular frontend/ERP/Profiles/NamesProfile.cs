using AutoMapper;
using ERP.Models;
using ERP.Dtos;

namespace ERP.Profiles
{
    public class NamesProfile : Profile
    {
        public NamesProfile()
        {
            CreateMap<Member, MemberReadDto>();
            CreateMap<MemberReadDto, Member>();

        }
    }
}