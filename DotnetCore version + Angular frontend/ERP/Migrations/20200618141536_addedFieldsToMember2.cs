﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Migrations
{
    public partial class addedFieldsToMember2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "By",
                table: "Members");

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Members",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                table: "Members");

            migrationBuilder.AddColumn<string>(
                name: "By",
                table: "Members",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);
        }
    }
}
