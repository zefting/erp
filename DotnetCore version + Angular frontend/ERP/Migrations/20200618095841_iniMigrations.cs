﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Migrations
{
    public partial class iniMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Boats",
                columns: table => new
                {
                    BoatId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Antal = table.Column<int>(nullable: false),
                    Bnavn = table.Column<string>(nullable: true),
                    Cox = table.Column<string>(nullable: true),
                    Inrig = table.Column<string>(nullable: true),
                    Outtype = table.Column<string>(nullable: true),
                    Idrift = table.Column<string>(nullable: true),
                    Orden = table.Column<int>(nullable: false),
                    Klub = table.Column<int>(nullable: false),
                    Kommentar = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Boats", x => x.BoatId);
                });

            migrationBuilder.CreateTable(
                name: "Destis",
                columns: table => new
                {
                    DestiId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Dnavn = table.Column<string>(nullable: true),
                    DestiDtoDestiId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Destis", x => x.DestiId);
                    table.ForeignKey(
                        name: "FK_Destis_Destis_DestiDtoDestiId",
                        column: x => x.DestiDtoDestiId,
                        principalTable: "Destis",
                        principalColumn: "DestiId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Members",
                columns: table => new
                {
                    MemberId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Medlemsnummer = table.Column<int>(nullable: false),
                    Fornavn = table.Column<string>(nullable: true),
                    Mellemnavn = table.Column<string>(nullable: true),
                    Efternavn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Members", x => x.MemberId);
                });

            migrationBuilder.CreateTable(
                name: "Rettigheders",
                columns: table => new
                {
                    RetId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RNavn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rettigheders", x => x.RetId);
                });

            migrationBuilder.CreateTable(
                name: "Turs",
                columns: table => new
                {
                    TripId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    BoatId = table.Column<int>(nullable: false),
                    Ltur = table.Column<bool>(nullable: false),
                    DestiId = table.Column<int>(nullable: false),
                    ForventetHjemkomst = table.Column<string>(nullable: true),
                    Hjemkomst = table.Column<string>(nullable: true),
                    Afgang = table.Column<string>(nullable: true),
                    StyrmandId = table.Column<int>(nullable: true),
                    KM = table.Column<int>(nullable: false),
                    Afsluttet = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Turs", x => x.TripId);
                    table.ForeignKey(
                        name: "FK_Turs_Boats_BoatId",
                        column: x => x.BoatId,
                        principalTable: "Boats",
                        principalColumn: "BoatId");
                    table.ForeignKey(
                        name: "FK_Turs_Destis_DestiId",
                        column: x => x.DestiId,
                        principalTable: "Destis",
                        principalColumn: "DestiId");
                    table.ForeignKey(
                        name: "FK_Turs_Members_StyrmandId",
                        column: x => x.StyrmandId,
                        principalTable: "Members",
                        principalColumn: "MemberId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RettighedMembers",
                columns: table => new
                {
                    RetId = table.Column<int>(nullable: false),
                    MemberId = table.Column<int>(nullable: false),
                    InstuctorId = table.Column<int>(nullable: false),
                    Dato = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RettighedMembers", x => new { x.RetId, x.InstuctorId, x.MemberId });
                    table.ForeignKey(
                        name: "FK_RettighedMembers_Members_InstuctorId",
                        column: x => x.InstuctorId,
                        principalTable: "Members",
                        principalColumn: "MemberId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RettighedMembers_Members_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Members",
                        principalColumn: "MemberId");
                    table.ForeignKey(
                        name: "FK_RettighedMembers_Rettigheders_RetId",
                        column: x => x.RetId,
                        principalTable: "Rettigheders",
                        principalColumn: "RetId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Turmedlems",
                columns: table => new
                {
                    MemberId = table.Column<int>(nullable: false),
                    TripId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Turmedlems", x => new { x.TripId, x.MemberId });
                    table.ForeignKey(
                        name: "FK_Turmedlems_Members_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Members",
                        principalColumn: "MemberId");
                    table.ForeignKey(
                        name: "FK_Turmedlems_Turs_TripId",
                        column: x => x.TripId,
                        principalTable: "Turs",
                        principalColumn: "TripId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Destis_DestiDtoDestiId",
                table: "Destis",
                column: "DestiDtoDestiId");

            migrationBuilder.CreateIndex(
                name: "IX_RettighedMembers_InstuctorId",
                table: "RettighedMembers",
                column: "InstuctorId");

            migrationBuilder.CreateIndex(
                name: "IX_RettighedMembers_MemberId",
                table: "RettighedMembers",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_Turmedlems_MemberId",
                table: "Turmedlems",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_Turs_BoatId",
                table: "Turs",
                column: "BoatId");

            migrationBuilder.CreateIndex(
                name: "IX_Turs_DestiId",
                table: "Turs",
                column: "DestiId");

            migrationBuilder.CreateIndex(
                name: "IX_Turs_StyrmandId",
                table: "Turs",
                column: "StyrmandId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RettighedMembers");

            migrationBuilder.DropTable(
                name: "Turmedlems");

            migrationBuilder.DropTable(
                name: "Rettigheders");

            migrationBuilder.DropTable(
                name: "Turs");

            migrationBuilder.DropTable(
                name: "Boats");

            migrationBuilder.DropTable(
                name: "Destis");

            migrationBuilder.DropTable(
                name: "Members");
        }
    }
}
