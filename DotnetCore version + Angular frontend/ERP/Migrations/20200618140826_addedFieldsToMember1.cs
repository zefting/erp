﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Migrations
{
    public partial class addedFieldsToMember1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fødelsdato",
                table: "Members");

            migrationBuilder.AddColumn<string>(
                name: "Bdato",
                table: "Members",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Bdato",
                table: "Members");

            migrationBuilder.AddColumn<string>(
                name: "Fødelsdato",
                table: "Members",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);
        }
    }
}
