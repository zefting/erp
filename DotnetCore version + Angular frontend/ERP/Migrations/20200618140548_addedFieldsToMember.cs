﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Migrations
{
    public partial class addedFieldsToMember : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Adresse",
                table: "Members",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "By",
                table: "Members",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Members",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Fødelsdato",
                table: "Members",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Postnummer",
                table: "Members",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Telefon",
                table: "Members",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Adresse",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "By",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "Fødelsdato",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "Postnummer",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "Telefon",
                table: "Members");
        }
    }
}
