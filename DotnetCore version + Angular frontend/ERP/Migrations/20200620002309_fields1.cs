﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Migrations
{
    public partial class fields1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RettighedMembers_Rettigheders_MemberId",
                table: "RettighedMembers");

            migrationBuilder.AddForeignKey(
                name: "FK_RettighedMembers_Rettigheders_RetId",
                table: "RettighedMembers",
                column: "RetId",
                principalTable: "Rettigheders",
                principalColumn: "RetId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RettighedMembers_Rettigheders_RetId",
                table: "RettighedMembers");

            migrationBuilder.AddForeignKey(
                name: "FK_RettighedMembers_Rettigheders_MemberId",
                table: "RettighedMembers",
                column: "MemberId",
                principalTable: "Rettigheders",
                principalColumn: "RetId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
