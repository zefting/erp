import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpretMemberModalComponent } from './opret-member-modal.component';

describe('OpretMemberModalComponent', () => {
  let component: OpretMemberModalComponent;
  let fixture: ComponentFixture<OpretMemberModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpretMemberModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpretMemberModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
