import { Component, OnInit,Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { faCalendarAlt, faAt } from '@fortawesome/free-solid-svg-icons';
import { faCopyright} from '@fortawesome/free-Regular-svg-icons';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, takeUntil,  filter, } from 'rxjs/operators';

@Component({
  selector: 'app-opret-member-modal',
  templateUrl: './opret-member-modal.component.html',
  styleUrls: ['./opret-member-modal.component.css']
})
export class OpretMemberModalComponent implements OnInit {
@Input() nytmember;
faCalendarAlt = faCalendarAlt;
  faAt = faAt;
  faCopyright = faCopyright;
  staticAlertClosed = true;
  message: string;

  constructor(public activeModal: NgbActiveModal,private httpClient: HttpClient) { }

  ngOnInit(): void {
    console.log(this.nytmember);
  }
  Opret(){
      this.httpClient.post('http://localhost:5000/api/members/', this.nytmember, {observe: 'response'})
      .subscribe(
        (response) => {
                        if (response.status === 200)
                            {
                              this.message = 'Medlemmet er oprettet';
                              this.staticAlertClosed = false;
                              setTimeout(() => this.staticAlertClosed = false, 5000);
                              console.log(response);
                            }
                      },
        (error) => {
                    this.message = 'Medlemmet kunne ikke oprettes' + (error);
                    this.staticAlertClosed = false;
                    setTimeout(() => this.staticAlertClosed = false, 5000);
                    console.log(error);
                    }
                );
  }
  
}
