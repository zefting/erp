import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-member-model',
  templateUrl: './member-model.component.html',
  styleUrls: ['./member-model.component.css'],

})
export class MemberModelComponent implements OnInit {
  modalModel: '' ;
  @Input() valgtMember;
  


  constructor(public activeModal: NgbActiveModal, private httpClient: HttpClient) { }

  closeModal() {
    this.activeModal.close('Modal Closed');
  }
  ngOnInit(): void {
    console.log('valgtmedlem', this.valgtMember);
  }
  update(){
    console.log(this.valgtMember);
    const updatedata =
       {
        adresse: this.valgtMember[0].adresse,
        bdato: this.valgtMember[0].bdato,
        city: this.valgtMember[0].city,
        efternavn: this.valgtMember[0].efternavn,
        email: this.valgtMember[0].email,
        fornavn: this.valgtMember[0]. fornavn,
        telefon: this.valgtMember[0].telefon,
        medlemsnummer: this.valgtMember[0].medlemsnummer,
        memberId: this.valgtMember[0].memberId,
        postnummer: this.valgtMember[0].postnummer ,
        rettighedMembers: this.valgtMember[0].rettighedMembers,
        ture: this.valgtMember[0].ture,
        turmedlems: this.valgtMember[0].turmedlems

       };
    console.log('updatedata', updatedata);
    this.httpClient.put('http://localhost:5000/api/members/updatemember/' + this.valgtMember[0].memberId, updatedata)
    .subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    );
    this.closeModal();
  }
}
