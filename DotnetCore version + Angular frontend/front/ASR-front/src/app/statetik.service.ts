import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { isNgTemplate } from '@angular/compiler';


@Injectable({
  providedIn: 'root'
})
export class StatetikService {
  private MemberAPi = 'http://localhost:5000/api/members/';
  private TurAPI = 'http://localhost:5000/api/members/';

  constructor(private httpClient: HttpClient) { }
  public sendMemGetRequest(){
    return this.httpClient.get(this.MemberAPi);
  }
  public sendMemberKmGetRequest(){
    return this.httpClient.get(this.MemberAPi);
  }
  public sendMemberByIdRequest(item){
    return this.httpClient.get(this.MemberAPi + 'getMemberById/' + item.memberId);
  }
}
