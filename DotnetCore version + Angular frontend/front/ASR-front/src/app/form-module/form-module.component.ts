import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-form-module',
  templateUrl: './form-module.component.html',
  styleUrls: ['./form-module.component.css']
})

export class FormModuleComponent implements OnInit {
  km: number;

  modalModel: '' ;
  @Input() valgttur;


  constructor(public activeModal: NgbActiveModal, private httpClient: HttpClient) { }
  closeModal() {
    this.activeModal.close('Modal Closed');
  }
  ngOnInit(): void {
  }
  update(){
    console.log(this.valgttur);
    const updatedata =
       {
        afsluttet: true,
        destiId: this.valgttur.destiId,
        afgang: this.valgttur.afgang,
        Hjemkomst: this.valgttur.forventetHjemkomst,
        boatId: this.valgttur.boatId,
        tripId: this.valgttur.tripId,
        km: this.km,

       };
    console.log(updatedata);
    this.httpClient.put('http://localhost:5000/api/turs/updateTur/' + this.valgttur.tripId, updatedata)
    .subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    );
    this.closeModal();
  }
}

