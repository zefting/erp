import { Component, Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, map, tap, switchMap} from 'rxjs/operators';
import {HttpClient, HttpParams} from '@angular/common/http';

const disti_url = 'http://localhost:5000/api/distis/';
const PARAMS = new HttpParams({
 fromObject: {
     action: 'opensearch',
     format: 'json',
     origin: '*'
 }
  });

@Injectable({
  providedIn: 'root'
})
export class DistiService {

  constructor(private http: HttpClient) {}

  search(term: string) {
    if (term === '') {
      return of();
    }

    return this.http.get(disti_url, {params: PARAMS.set('search', term)}).pipe(
      map(response => response, {dnavn : 'dnavn'}));
}
}
