import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { isNgTemplate } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class RettighederService {
  private RetAPi = 'http://localhost:5000/api/rettigheder/GetAllRettigheder';
  private MemberAPi = 'http://localhost:5000/api/members/';
  private CoxAPi = 'http://localhost:5000/api/members/';

  constructor(private httpClient: HttpClient) { }

  public sendRetGetRequest(){
    return this.httpClient.get(this.RetAPi);
  }
  public sendMemGetRequest(){
    return this.httpClient.get(this.MemberAPi);
  }
  public sendMemberByIdRequest(value){
    return this.httpClient.get(this.MemberAPi + 'getMemberById/' + value);
  }
  public sendCoxGetRequest(){
    return this.httpClient.get(this.CoxAPi);
  }

  public putMemberChangeRequest(item){
    const updatedata = {

    };

    return this.httpClient.put(this.MemberAPi + item.memberId, updatedata)
    .subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    );
  }

}
