import { Component, OnInit, Input  } from '@angular/core';
import { RettighederService } from '../rettigheder.service';
import { FormsModule } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

import { MemberModelComponent } from './../member-model/member-model.component';
@Component({
  selector: 'app-stamkort',
  templateUrl: './stamkort.component.html',
  styleUrls: ['./stamkort.component.css']

})
export class StamkortComponent implements OnInit {
  rettighederOversigt = [];

  MemberById: any;
  value: any;
  constructor(private rettighederService: RettighederService, private modalService: NgbModal, private route: ActivatedRoute) {

    this.route.params.subscribe(params => {
      this.value = params.memberId; // --> Name must match wanted parameter
    });
    console.log(this.value);
    this.setrettighedoversigt();
   }


  // rettighedsdefinationer
   setrettighedoversigt(){
     // Henter rettighedsdefinationer
     this.rettighederService.sendRetGetRequest().subscribe((rdata: any[]) => {
      this.rettighederOversigt = rdata;
      console.log('rettighederOversigt', this.rettighederOversigt);
      });
   }

  ngOnInit(): void {



   this.rettighederService.sendMemberByIdRequest(this.value).subscribe((mIddata: any) => {
    this.MemberById = mIddata;
    console.log('memberById', this.MemberById);
    });
  }
  //
  onmItemSelect(item: any) {

    }
  openMemberModal() {
     const modalRef = this.modalService.open(MemberModelComponent, { size: 'lg' });
     modalRef.componentInstance.valgtMember = this.MemberById;
     console.log(this.MemberById);
    }
}
