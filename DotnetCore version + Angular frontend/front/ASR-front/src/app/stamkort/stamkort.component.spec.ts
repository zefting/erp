import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StamkortComponent } from './stamkort.component';

describe('StamkortComponent', () => {
  let component: StamkortComponent;
  let fixture: ComponentFixture<StamkortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StamkortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StamkortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
