import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MedlemComponent } from './medlem/medlem.component';
import { OpretFormComponent } from './opret-form/opret-form.component';
import { GivRettighedComponent } from './giv-rettighed/giv-rettighed.component';
import { StamkortComponent } from './stamkort/stamkort.component';
import { HomeComponent } from './home/home.component';
import { OpretmedlemComponent } from './opretmedlem/opretmedlem.component';
import { StatetikComponent } from './statetik/statetik.component';




const routes: Routes = [
  { path: '',  redirectTo: '/home', pathMatch: 'full' },
  {path: 'home' , component: HomeComponent},

  {path: 'ebs' , component: OpretFormComponent},

  {path: 'rettighed' , component: GivRettighedComponent},

  {path: 'stamkort/:memberId' , component: StamkortComponent},

  {path: 'opret-medlem' , component: OpretmedlemComponent},

  {path: 'statetik' , component: StatetikComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
