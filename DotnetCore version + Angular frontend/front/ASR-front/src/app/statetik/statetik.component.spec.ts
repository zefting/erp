import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatetikComponent } from './statetik.component';

describe('StatetikComponent', () => {
  let component: StatetikComponent;
  let fixture: ComponentFixture<StatetikComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatetikComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatetikComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
