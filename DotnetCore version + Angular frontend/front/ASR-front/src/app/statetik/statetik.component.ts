import { Component, OnInit } from '@angular/core';
import { StatetikService } from './../statetik.service';

@Component({
  selector: 'app-statetik',
  templateUrl: './statetik.component.html',
  styleUrls: ['./statetik.component.css']
})
export class StatetikComponent implements OnInit {
  conmdata = [];
  mdropdownList = [];
  mselectedItems = [];
  mdropdownSettings = {};
  MemberById = [];
  selectedItems = [];

  constructor(private _StatestikService: StatetikService) { }
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
  ];
  MedlemsOversigt = [];

  ngOnInit(): void {
    // henter medlemsinformationer
   this._StatestikService.sendMemGetRequest().subscribe((mdata: any[]) => {
     this.MedlemsOversigt = mdata;

     for (let navn of mdata){

       const fuldnavn = navn.medlemsnummer +  ' ' + navn.fornavn + ' ' + navn.efternavn;
       navn.fuldnavn = fuldnavn;

     }
     console.log('MedlemsOversigt', this.MedlemsOversigt);
     // Temp til valg af medlem
     this.mdropdownList = mdata;
     this.mdropdownSettings = {
     singleSelection: true,
     idField: 'memberId',
     textField: 'fuldnavn',
     itemsShowLimit: 3,
     enableCheckAll: false,
     allowSearchFilter: true

   };
     });
  }
  onmItemSelect(item: any) {
    this._StatestikService.sendMemberByIdRequest(item).subscribe((mIddata: any[]) => {
      this.MemberById = [mIddata];
      console.log('memberById', this.MemberById);
      });
    }
    
}
