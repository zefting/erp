import { TestBed } from '@angular/core/testing';

import { DistiService } from './disti-service.service';

describe('DistiService', () => {
  let service: DistiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DistiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
