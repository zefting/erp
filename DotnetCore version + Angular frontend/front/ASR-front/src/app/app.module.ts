import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {NgForm, FormBuilder, FormGroup, FormControl, ReactiveFormsModule} from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MedlemComponent } from './medlem/medlem.component';
import { OpretControllerComponent } from './opret-controller/opret-controller.component';
import { OpretFormComponent } from './opret-form/opret-form.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CommonModule, DatePipe } from '@angular/common';
import { FormModuleComponent } from './form-module/form-module.component';
import { LOCALE_ID } from '@angular/core';
import localeDa from '@angular/common/locales/da';
import { GivRettighedComponent } from './giv-rettighed/giv-rettighed.component';
import { StamkortComponent } from './stamkort/stamkort.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
// https://github.com/danrevah/ngx-pipes
import {NgPipesModule} from 'ngx-pipes';
import { MemberModelComponent } from './member-model/member-model.component';
import { HomeComponent } from './home/home.component';
import { OpretmedlemComponent } from './opretmedlem/opretmedlem.component';
import { OpretMemberModalComponent } from './opret-member-modal/opret-member-modal.component';
import { ChartsModule } from 'ng2-charts';
import { StatetikComponent } from './statetik/statetik.component';


@NgModule({
  declarations: [
    AppComponent,
    MedlemComponent,
    OpretControllerComponent,
    OpretFormComponent,
    FormModuleComponent,
    GivRettighedComponent,
    StamkortComponent,
    MemberModelComponent,
    HomeComponent,
    OpretmedlemComponent,
    OpretMemberModalComponent,
    StatetikComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    NgPipesModule,
    HttpClientModule,
    FontAwesomeModule,
    NgMultiSelectDropDownModule.forRoot(),
    BrowserAnimationsModule,
    CommonModule,
    ChartsModule
  ],
  providers: [DatePipe, { provide: LOCALE_ID, useValue: 'da-DK' }],
  bootstrap: [AppComponent]
})
export class AppModule { }


