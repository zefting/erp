import { Component, OnInit } from '@angular/core';
import { faCalendarAlt, faAt } from '@fortawesome/free-solid-svg-icons';
import { faCopyright} from '@fortawesome/free-Regular-svg-icons';
import * as $ from 'jquery';
import 'bootstrap';
import {NgbDateStruct, NgbCalendar, NgbTimeAdapter, NgbTabsetConfig, NgbDateParserFormatter, NgbDateAdapter} from '@ng-bootstrap/ng-bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OpretMemberModalComponent } from './../opret-member-modal/opret-member-modal.component';

@Component({
  selector: 'app-opretmedlem',
  templateUrl: './opretmedlem.component.html',
  styleUrls: ['./opretmedlem.component.css']
})
export class OpretmedlemComponent implements OnInit {
  faCalendarAlt = faCalendarAlt;
  faAt = faAt;
  faCopyright = faCopyright;

  member = {
            telefon: null,
            adresse: null,
            startDateModel: null,
            fornavn: null,
            mellemnavn:null,
            efternavn:null,
            postnummer:null,
            city:null,
            email:null,
            concent:false,
            tklub:false,
            tklubnavn:null

          };

  constructor(private calendar: NgbCalendar, private dateAdapter: NgbDateAdapter<string>, private modalService: NgbModal) { }

  ngOnInit(): void {
  }
  selectStartDateToday() {
    this.member.startDateModel = this.dateAdapter.toModel(this.calendar.getToday());
    console.log(this.member.startDateModel);
  }
  openOpretMemberModal() {
    const modalRef = this.modalService.open(OpretMemberModalComponent, { size: 'lg' });
    modalRef.componentInstance.nytmember = this.member;
    console.log(this.member);
   }


}
