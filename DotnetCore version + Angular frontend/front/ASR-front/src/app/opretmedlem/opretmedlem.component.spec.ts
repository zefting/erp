import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpretmedlemComponent } from './opretmedlem.component';

describe('OpretmedlemComponent', () => {
  let component: OpretmedlemComponent;
  let fixture: ComponentFixture<OpretmedlemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpretmedlemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpretmedlemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
