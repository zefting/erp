import { Component, OnInit } from '@angular/core';
import { faCalendarAlt, faAt } from '@fortawesome/free-solid-svg-icons';
import { faCopyright} from '@fortawesome/free-Regular-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  faCalendarAlt = faCalendarAlt;
  faAt = faAt;
  faCopyright = faCopyright;
  constructor() { }

  ngOnInit(): void {
  }

}
