import {Component, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, map, tap, switchMap} from 'rxjs/operators';
import { ReactiveFormsModule } from '@angular/forms';
// import {MedlemComponemt} from './../medlem/medlem.component';
declare var antal: any;
const boat_url = 'http://localhost:5000/api/boats/';
const PARAMS = new HttpParams({
 fromObject: {
     action: 'opensearch',
     format: 'json',
     origin: '*'
 }
  });

@Injectable({
  providedIn: 'root'
})
export class BoatTypeService {
  constructor(private http: HttpClient) {}

  search(term: string) {
    if (term === '') {
      return of();
    }

    return this.http.get(boat_url, {params: PARAMS.set('boatsearch', term)}).pipe(
      map(response => response, {bnavn : 'bnavn'}));
}/*
}
@Component({
  selector: 'app-root',
  templateUrl: './boat-type.component.html',
  providers: [BoatTypeService]
})

export class BoatTypeComponent {

  model: any;
  searching = false;
  searchFailed = false;
  clickedItem: string;
  constructor(private _service: BoatTypeService) {}
  search = (text$: Observable<any>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(term =>
        this._service.search(term).pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          }))
      ),
      tap(() => this.searching = false)
    )
    selectedItem(item){
      this.clickedItem = item;
      console.log(item);
      let getKeys = Object.values(item);
      let getKey = getKeys[0];
      var antal = getKey['antal'];
      console.log(getKey);
      console.log(antal);
      return antal;
    }
   formatMatches = (x: {bnavn: string}) => x.bnavn;
}

*/
}