import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedlemComponent } from './medlem.component';

describe('MedlemComponent', () => {
  let component: MedlemComponent;
  let fixture: ComponentFixture<MedlemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedlemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedlemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
