import {Component, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, map, tap, switchMap} from 'rxjs/operators';

const medlem_url = 'http://localhost:5000/api/members/';
const PARAMS = new HttpParams({
 fromObject: {
     action: 'opensearch',
     format: 'json',
     origin: '*'
 }
  });

@Injectable({
    providedIn: 'root'
  })
  export class MedlemService {
    constructor(private http: HttpClient) {}
    search(term: string) {
      if (term === '') {
        return of();
      }
      return this.http.get(medlem_url, {params: PARAMS.set('search', term)}).pipe(
        map(response => response, {fornavn : 'fornavn'}));
  }
  }
@Component({
    selector: 'app-root',
    templateUrl: './medlem.component.html',
    providers: [MedlemService],
    styles: [`.form-control { width: 300px; }`]
  })
  export class MedlemComponent {
    public membername: any;
    modelName: any;
    searching = false;
    searchFailed = false;
    clickedNameItem: string;
    constructor(private _service: MedlemService) {}
    search = (text$: Observable<any>) =>
      text$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => this.searching = true),
        switchMap(term =>
          this._service.search(term).pipe(
            tap(() => this.searchFailed = false),
            catchError(() => {
              this.searchFailed = true;
              return of([]);
            }))
        ),
        tap(() => this.searching = false)
      )
      selectedNameItem(navnitem){
        this.clickedNameItem = navnitem;
        console.log(navnitem);
        let getnavnKeys = Object.values(navnitem);
        const getnavnkey = getnavnKeys[0];
        this.membername = getnavnkey['nameId'];
        console.log(this.membername);
      }
     formatMatches = (x: {fornavn: string, efternavn: string}) => x.fornavn + ' ' + x.efternavn;
  }
