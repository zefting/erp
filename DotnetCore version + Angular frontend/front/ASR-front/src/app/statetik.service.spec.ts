import { TestBed } from '@angular/core/testing';

import { StatetikService } from './statetik.service';

describe('StatetikService', () => {
  let service: StatetikService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StatetikService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
