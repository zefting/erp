import { TestBed } from '@angular/core/testing';

import { TimeadapterService } from './timeadapter.service';

describe('TimeadapterService', () => {
  let service: TimeadapterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TimeadapterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
