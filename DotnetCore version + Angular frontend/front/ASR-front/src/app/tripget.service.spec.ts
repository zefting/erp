import { TestBed } from '@angular/core/testing';

import { TripgetService } from './tripget.service';

describe('TripgetService', () => {
  let service: TripgetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TripgetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
