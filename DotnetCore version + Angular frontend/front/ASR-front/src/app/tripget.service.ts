import { Component, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TripgetService {
  private apiServer = 'http://localhost:5000/api/turs/GetCurrentTurs/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private httpClient: HttpClient) { }
  get(): Observable<any[]> {
    return this.httpClient.get<any[]>(this.apiServer);
  }
  }
