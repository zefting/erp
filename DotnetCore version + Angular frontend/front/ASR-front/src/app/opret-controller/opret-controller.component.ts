import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import { OpretFormComponent } from './../opret-form/opret-form.component';

@Component({
  selector: 'app-opret-controller',
  templateUrl: './opret-controller.component.html',
  styleUrls: ['./opret-controller.component.css'],
  providers: [OpretFormComponent],
})
export class OpretControllerComponent implements OnInit {
  form: FormGroup;
  desti: {};
  boat: {};
  member: any;
  constructor(private httpClient: HttpClient, private opretform: OpretFormComponent) { }
  ngOnInit() { }
  modelDestiConvert(){
    let getdestiKeys = Object.values(this.opretform.model.destiModel);
    const getdestikey = getdestiKeys[0];
    this.desti = getdestikey['destiId'];
  }
  modelBoatConvert(){
    let getboatKeys = Object.values(this.opretform.model.modelBoat);
    const getboatkey = getboatKeys[0];
    this.boat = getboatkey['boatId'];
  }
  modelMemberConvert(){
    for (let typenavnId in this.opretform.model.modelName) {
    if (this.opretform.model.modelName.hasOwnProperty)
    {let getmemberKeys = Object.values(this.opretform.model.modelName);
     const getmemberkey = getmemberKeys[0];
     this.boat = getmemberkey['typenavnId'];
     this.member.push(Number(typenavnId));
    }
    }
  }
  sendPostRequest(){
    this.modelDestiConvert();
    this.modelBoatConvert();
    this.modelMemberConvert();
    var formData: any = new FormData();
    // bliver det hentet fra modellen i funktionen, eller skal jeg kalde lave funktionen her og bruge dataen?
    formData.append('BoatId', this.boat);
    formData.append('DestiId', this.desti);
    formData.append('Starttid', this.opretform.model.startDateModel);
    formData.append('ForventetHjemkost', this.opretform.model.slutDateModel);
    formData.append('typenavnId', this.opretform.namemember);
    console.log(formData);
    this.httpClient.post('http://localhost:5000/api/turs/', formData)
    .subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    );
}
}

