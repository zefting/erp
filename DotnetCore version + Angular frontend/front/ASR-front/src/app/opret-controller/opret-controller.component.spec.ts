import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpretControllerComponent } from './opret-controller.component';

describe('OpretControllerComponent', () => {
  let component: OpretControllerComponent;
  let fixture: ComponentFixture<OpretControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpretControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpretControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
