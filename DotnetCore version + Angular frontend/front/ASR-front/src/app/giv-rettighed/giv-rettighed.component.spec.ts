import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GivRettighedComponent } from './giv-rettighed.component';

describe('GivRettighedComponent', () => {
  let component: GivRettighedComponent;
  let fixture: ComponentFixture<GivRettighedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GivRettighedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GivRettighedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
