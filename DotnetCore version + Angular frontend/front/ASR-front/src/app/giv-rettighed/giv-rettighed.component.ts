import { Component, OnInit, Input  } from '@angular/core';
import { RettighederService } from '../rettigheder.service';



@Component({
  selector: 'app-giv-rettighed',
  templateUrl: './giv-rettighed.component.html',
  styleUrls: ['./giv-rettighed.component.css']
})
export class GivRettighedComponent implements OnInit {
  @Input() name: string;
  // rettigheder
  
  retdropdownList = [];
  retselectedItems = [];
  retdropdownSettings = {};
  retselected: any[];
  selectedItems = [];
   // medlemmer
   conmdata = [];
   mdropdownList = [];
   mselectedItems = [];
   mdropdownSettings = {};
  constructor(private _RettighederService: RettighederService) { }
  ngOnInit() {
   
    this.selectedItems = [
  ];
    // rettigheder
    this._RettighederService.sendRetGetRequest().subscribe((rdata: any[]) => {
      console.log(rdata);
      this.retdropdownList = rdata;
  });
    this.retselectedItems = [];
    console.log(this.retselectedItems);
    this.retdropdownSettings = {
      singleSelection: false,
      idField: 'retId',
      textField: 'rNavn',
      enableCheckAll: false,
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  // medlemmer
    this._RettighederService.sendMemGetRequest().subscribe((mdata: any[]) => {
    console.log('medlemmer array');
    console.log(mdata);
    this.mdropdownList = mdata;
    for (let navn of mdata){

      const fuldnavn = navn.medlemsnummer +  ' ' + navn.fornavn + ' ' + navn.efternavn;
      navn.fuldnavn = fuldnavn;

    }});

    this.mselectedItems = [];
    this.mdropdownSettings = {
        singleSelection: false,
        idField: 'memberId',
        textField: 'fuldnavn',
        itemsShowLimit: 3,
        enableCheckAll: false,
        allowSearchFilter: true
      };
    }
  onrItemSelect(item: any) {
    
    console.log('onItemSelect', item);
    console.log('retselectedItems', this.retselectedItems);

}
  onmItemSelect(item: any) {
  console.log('onItemSelect', item);
}
}
