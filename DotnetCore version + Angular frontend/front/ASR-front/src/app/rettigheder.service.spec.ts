import { TestBed } from '@angular/core/testing';

import { RettighederService } from './rettigheder.service';

describe('RettighederServiceService', () => {
  let service: RettighederService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RettighederService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
