import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormModuleComponent } from './../form-module/form-module.component';
import {NgbTimeStringAdapter} from './../timeadapter.service';
import {BoatTypeService} from './../boat-type/boat-type.component';
import { MedlemService } from './../medlem/medlem.component';
import { TripgetService } from './../tripget.service';
import {DistiService} from './../disti-service.service';
import {CustomAdapter, CustomDateParserFormatter } from './../dateadapter.service';
import * as $ from 'jquery';
import 'bootstrap';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, takeUntil,  filter, } from 'rxjs/operators';
import { ReactiveFormsModule } from '@angular/forms';
import {NgbDateStruct, NgbCalendar, NgbTimeAdapter, NgbTabsetConfig, NgbDateParserFormatter, NgbDateAdapter} from '@ng-bootstrap/ng-bootstrap';
import { faCalendarAlt, faAt } from '@fortawesome/free-solid-svg-icons';
import { faCopyright} from '@fortawesome/free-Regular-svg-icons';



import { Pipe, PipeTransform } from '@angular/core';
import {NgForm} from '@angular/forms';
import { variable } from '@angular/compiler/src/output/output_ast';
import { DatePipe, registerLocaleData } from '@angular/common';
import { CommonModule } from '@angular/common';

import localeDa from '@angular/common/locales/da';
registerLocaleData(localeDa, 'da-DK');
// fra https://stackblitz.com/edit/angular-dyanmic-fields-form-array
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: { day: 'numeric', month: 'numeric', year: 'numeric', hour: 'numeric', minute: 'numeric'}
};

@Component({
  selector: 'app-opret-form',
  templateUrl: './opret-form.component.html',
  styleUrls: ['./opret-form.component.css'],
  providers: [ BoatTypeService, MedlemService, DistiService, TripgetService, {provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter},
    {provide: NgbDateAdapter, useClass: CustomAdapter},
    {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter}]
})

export class OpretFormComponent  implements OnInit, OnDestroy, OnChanges {
  faCalendarAlt = faCalendarAlt;
  faAt = faAt;
  faCopyright = faCopyright;
  modalContent: undefined;
  desti: [];
  boat: [];
  public coxConvert: any;
  cox: number;
  member = [];
  allturdata = [];
  public antal: any;
  public membername: any;
  public namemember: Array<string> = [];
  model = {modelBoat: [], modelName: [], startDateModel: {}, slutDateModel: {},
  Starttid: {}, slutTid: {}, destiModel: [], modelCox: {}, ModelLtur: false};
  staticAlertClosed = true;
  searching = false;
  searchFailed = false;
  message: string;
  pipe = new DatePipe('da-DK');
  tidtoday = new Date();
  clickeddestiItem: string;
  clickedboatItem: string;
  clickedNameItem: string;
  clickedCoxItem: string;
  public form: FormGroup;
  private destroy$ = new Subject();

  constructor(private modalService: NgbModal, private httpClient: HttpClient, private _boatService: BoatTypeService,
              private medlemcomponent: MedlemService, private _distiService: DistiService, private _tripgetService: TripgetService,
              private calendar: NgbCalendar, private dateAdapter: NgbDateAdapter<string>
              ) {}
  ngOnChanges(){
  }
  boatsearch = (text$: Observable<any>) =>
  text$.pipe(
    debounceTime(300),
    distinctUntilChanged(),
    tap(() => this.searching = true),
    switchMap(term =>
      this._boatService.search(term).pipe(
        tap(() => this.searchFailed = false),
        catchError(() => {
          this.searchFailed = true;
          return of([]);
        }))
    ),
    tap(() => this.searching = false)
  )
  generateResourceForm() {
    this.resources.clear();
    for (let x = 0; x < this.resourceQuantity; x++) {
      this.namemember.push('');
      this.resources.push(this.createResource());
    }
  }
  selectedBoatItem(bitem){
    this.antal = bitem.item.antal;
    this.boat = bitem.item.boatId;
    this.cox = bitem.item.cox;
    this.generateResourceForm();
  }

  getCurentTure() {
    this.allturdata = [];
    this._tripgetService.get().subscribe((allturdata: any[]) => {
    this.allturdata = allturdata;
    console.log(this.allturdata);
  });
  }
 formatMatches = (x: {bnavn: string}) => x.bnavn;
 // create form based on input
  ngOnInit() {
    this.form = new FormGroup({
      boat: new FormControl('boat'),
      'resources': new FormArray([])
    });
    this.getCurentTure();
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  createResource() {
    return new FormGroup({
      id: new FormControl(null, { validators: Validators.required})
    });
  }
  add() {
    console.log(this.form.value);
  }

  get resourceQuantity() {
    return this.antal;
  }
  get resources() {
    return this.form.get('resources') as FormArray;
  }

 Namemember(id) {
    return this.namemember[id];
  }
  selectedNameItem(navnitem){
    this.clickedNameItem = navnitem;
    console.log(navnitem);
    let getnavnKeys = Object.values(navnitem);
    const getnavnkey = getnavnKeys[0];
    this.membername = getnavnkey['memberId'];
    console.log(this.membername);
  }
  selectedCoxItem(coxitem){
    this.clickedCoxItem = coxitem;
    console.log(coxitem);
    let getnavnKeys = Object.values(coxitem);
    const getnavnkey = getnavnKeys[0];
    this.coxConvert = getnavnkey['memberId'];
    console.log('cox');
    console.log(this.coxConvert);
  }

 namesearch = (text$: Observable<any>) =>
  text$.pipe(
    debounceTime(300),
    distinctUntilChanged(),
    tap(() => this.searching = true),
    switchMap(term =>
      this.medlemcomponent.search(term).pipe(
        tap(() => this.searchFailed = false),
        catchError(() => {
          this.searchFailed = true;
          return of([]);
        }))
    ),
    tap(() => this.searching = false)
  )
  formatNameMatches = (x: {fornavn: string, efternavn: string}) => x.fornavn + ' ' + x.efternavn;
  formatNameInputMatches = (x: {fornavn: string, efternavn: string}) => x.fornavn + ' ' +  x.efternavn;
  formatCoxMatches = (x: {fornavn: string, efternavn: string}) => x.fornavn + ' ' + x.efternavn;
  formatCoxInputMatches = (x: {fornavn: string, efternavn: string}) => x.fornavn + ' ' +  x.efternavn;
  formatDestiMatches = (x: {dnavn: string}) => x.dnavn;
  formatDestiInputMatches = (x: {dnavn: string}) => x.dnavn;
// skal ændres til https://weblog.west-wind.com/posts/2019/Apr/08/Using-the-ngBootStrap-TypeAhead-Control-with-Dynamic-Data
  destisearch = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(300),
    distinctUntilChanged(),
    tap(() => this.searching = true),
    switchMap(term =>
      this._distiService.search(term).pipe(
        tap(() => this.searchFailed = false),
        catchError(() => {
          this.searchFailed = true;
          return of([]);
        }))
    ),
    tap(() => this.searching = false)
  )

  selectedDestiItem(destiitem){
    console.log(destiitem);
    this.desti = destiitem.item.destiId;
    console.log(this.desti);
  }
  modelMemberConvert(){
    const getmemberKeys = Object.values(this.model.modelName);
    // henter value fra objectet ud af listen
    console.log(getmemberKeys);
    console.log("medlem:")
    getmemberKeys.forEach(({memberId}) => {
      let memberpush = this.member.push({"memberId": memberId});
      console.log(memberpush);
      console.log(this.member);
      });
  }
  toggleltur() {
    this.model.ModelLtur = !this.model.ModelLtur;
  }
  sendPostRequest(){
    this.modelMemberConvert();
    const data =
    {
      "BoatId": this.boat,
      "DestiId": this.desti,
      "forventetHjemkomst":  String(this.model.slutDateModel)  + ' ' + this.model.slutTid,
      "Afgang": String(this.model.startDateModel) + ' ' + this.model.Starttid,
      "Ltur": this.model.ModelLtur,
      "Turmedlems": this.member,
    //"Styrmand": this.coxConvert
    };
    console.log(data);
    this.form.reset();
    console.log(this.form);

    this.httpClient.post('http://localhost:5000/api/turs/', data, {observe: 'response'})
    .subscribe(
      (response) => {
                      if (response.status === 200)
                          {
                            this.message = 'Turen er oprettet';
                            this.staticAlertClosed = false;
                            setTimeout(() => this.staticAlertClosed = false, 5000);
                            console.log(response);
                          }
                    },
      (error) => {
                  this.message = 'Turen kunne ikke oprettes' + (error);
                  this.staticAlertClosed = false;
                  setTimeout(() => this.staticAlertClosed = false, 5000);
                  console.log(error);
                  }
              );
}
openFormModal(tur) {
  const modalRef = this.modalService.open(FormModuleComponent, { size: 'xl' });
  modalRef.componentInstance.valgttur = tur;
}
selectStartDateToday() {
  this.model.startDateModel = this.dateAdapter.toModel(this.calendar.getToday());
  console.log(this.model.startDateModel);
}

selectafgangnu() {
  console.log(this.tidtoday);
  this.model.Starttid = this.pipe.transform(this.tidtoday, 'hh:mm:ss');
  console.log(this.model.Starttid);
}

selectSlutDateToday() {
  this.model.slutDateModel = this.dateAdapter.toModel(this.calendar.getToday());
}
ngAfterViewInit() {
  $('[data-toggle="tooltip"]').tooltip({sanitize: false, sanitizeFn: content => content});
}
}
