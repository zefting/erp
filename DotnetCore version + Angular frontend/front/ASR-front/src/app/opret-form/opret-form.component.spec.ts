import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpretFormComponent } from './opret-form.component';

describe('OpretFormComponent', () => {
  let component: OpretFormComponent;
  let fixture: ComponentFixture<OpretFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpretFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpretFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
