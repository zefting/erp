import { TestBed } from '@angular/core/testing';

import { DateadapterService } from './dateadapter.service';

describe('DateadapterService', () => {
  let service: DateadapterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DateadapterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
