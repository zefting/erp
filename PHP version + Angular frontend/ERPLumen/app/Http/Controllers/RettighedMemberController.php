<?php

namespace App\Http\Controllers;

use App\RettighedMember;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;


class RettighedMemberController extends Controller
{

    public function showAllRettighedmembers()
    {  
        $members = DB::table('members')
        ->join('rettighed_members', 'members.members_id' , '=',  'rettighed_members.members_id')
        ->select(
            'rettighed_members.rettigheders_id', 
            'rettighed_members.dato', 
            'members.fornavn', 
            'members.mellemnavn', 
            'members.efternavn', 
            'members.medlemsnummer', 
            'members.adresse',
            'members.email',
            'members.postnummer',
            'members.telefon',
            'members.bdato',
            'members.city',
            )    
        ->get();
        

      
        return response()->json($members);
        
    }

    public function showAllRettighedmember($members_id)
    {   


        return response()->json(RettighedMember::where('members_id', $members_id)->select('instuctor_id', 'Dato', 'rettigheders_id')->get()->groupBy('members_id'));
         
    
    }

    public function create(Request $request)
    {
        $rettighed = RettighedMember::create($request->all());

        return response()->json($rettighed, 201);
    }

    
    public function update($id, Request $request)
    {
        $rettighed = RettighedMember::findOrFail($id);
        $rettighed->update($request->all());

        return response()->json($rettighed, 200);
    }

    public function delete($id)
    {
        RettighedMember::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}