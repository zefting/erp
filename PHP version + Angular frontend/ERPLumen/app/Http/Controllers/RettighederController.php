<?php

namespace App\Http\Controllers;

use App\Rettigheders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RettighederController extends Controller
{

    public function showAllRettigheders()
    {
        return response()->json(Rettigheders::all());
    }

    public function showAllRettigheder($rettigheders_id)
    {
        return response()->json(Rettigheders::find($rettigheders_id));
    }

    public function create(Request $request)
    {
        $rettighed = Rettigheders::create($request->all());

        return response()->json($rettighed, 201);
    }

    public function update($rettigheders_id, Request $request)
    {
        $rettighed = Rettigheders::findOrFail($rettigheders_id);
        $rettighed->update($request->all());

        return response()->json($rettighed, 200);
    }

    public function delete($rettigheders_id)
    {
        Rettigheders::findOrFail($rettigheders_id)->delete();
        return response('Deleted Successfully', 200);
    }
}