<?php

namespace App\Http\Controllers;

use App\Members;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

class MemberController extends Controller
{

    public function showAllMembers()
    {
        return response()->json(Members::all());
    }

    public function showOneMember($members_id)
    {
        return response()->json(Members::find($members_id));
    }
    public function showOneMemberRet($members_id)
    {
        
            $users = DB::table('rettighed_members')
           // ->join('members', 'members.members_id', '=', 'rettighed_members.members_id')
            //->where('members.members_id', '=', $members_id)
            ->select('rettighed_members.members_id', 'rettighed_members.rettigheders_id' )
            
            ->get();
           return $users;
    }
      
            /*return $users->map(function($item, $key)
            {   
                return $item . $key;
            });
            
            
               
                    
           


                        
        }
    
            

            //Members::find($members_id)->rettighedMember()->first());
            
*/

    public function create(Request $request)
    {
        $member = Members::create($request->all());

        return response()->json($member, 201);
    }

    public function update($members_id, Request $request)
    {
        $member = Members::findOrFail($members_id);
        $member->update($request->all());

        return response()->json($member, 200);
    }

    public function delete($members_id)
    {
        Members::findOrFail($members_id)->delete();
        return response('Deleted Successfully', 200);
    }
}