<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Members extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'members_id';
    protected $fillable = [
        'Medlemsnummer', 
        'Fornavn', 
        'Mellemnavn', 
        'Efternavn', 
        'Adresse', 
        'Email',
        'Postnummer', 
        'Telefon', 
        'Bdato', 
        'City'
    ];
    public function rettighedMember()
    {
        return $this->hasMany('App\RettighedMember', 'member_id', 'member_id');
    }
   

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}