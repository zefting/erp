<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rettigheders extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'rettigheders_id';
    protected $fillable = [
        'Rnavn' 
       
    ];
    public function members ()
    {
        return $this->belongsTo('App\RettighedMember');
    }
    
    

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}