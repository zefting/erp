<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RettighedMember extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'rettigheders_id', 
        'members_id', 
        'instuctor_id', 
        'Dato'
    ];
    
   public function rettigheder()
    {
        return $this->belongsTo('App\Members');
    }
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}