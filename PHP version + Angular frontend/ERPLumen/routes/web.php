<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
// member info 
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('members',  ['uses' => 'MemberController@showAllMembers']);
  
    $router->get('members/{members_id}', ['uses' => 'MemberController@showOneMember']);
    $router->get('members/memberret/{members_id}', ['uses' => 'MemberController@showOneMemberRet']);
   
  
    $router->post('members', ['uses' => 'MemberController@create']);
  
    $router->delete('members/{members_id}', ['uses' => 'MemberController@delete']);
  
    $router->put('members/{members_id}', ['uses' => 'MemberController@update']);
  });
  // cox-rights
  $router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('rettigheders',  ['uses' => 'RettighederController@showAllRettigheders']);
  
    $router->get('rettigheders/{rettigheders_id}', ['uses' => 'RettighederController@showAllRettigheder']);
  
    $router->post('rettigheders', ['uses' => 'RettighederController@create']);
  
    $router->delete('rettigheders/{rettigheders_id}', ['uses' => 'RettighederController@delete']);
  
    $router->put('rettigheders/{rettigheders_id}', ['uses' => 'RettighederController@update']);
  });
  // Relations between cox-right and members
  $router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('rettighedmember',  ['uses' => 'rettighedmemberController@showAllRettighedmembers']);
  
    $router->get('rettighedmember/{members_id}', ['uses' => 'rettighedmemberController@showAllRettighedmember']);
  
    $router->post('rettighedmember', ['uses' => 'rettighedmemberController@create']);
    
    
  
    $router->delete('rettighedmember/{id}', ['uses' => 'rettighedmemberController@delete']);
  
    $router->put('rettighedmember/{id}', ['uses' => 'rettighedmemberController@update']);
  });
  $router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});