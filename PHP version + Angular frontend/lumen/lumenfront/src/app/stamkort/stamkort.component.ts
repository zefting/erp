import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { RestService } from '../rest.service';


@Component({
  selector: 'app-stamkort',
  templateUrl: './stamkort.component.html',
  styleUrls: ['./stamkort.component.css']
})
export class StamkortComponent implements OnInit {
  retId: any;


  // temp router
  conmdata = [];
  mdropdownList = [];
  mselectedItems = [];
  mdropdownSettings = {};
  MemberById = [];
  selectedItems = [];

  constructor(private restService: RestService, private modalService: NgbModal) { }
  // holder alle rettigheder
  rettighederOversigt = [];

  // holder medlemsoplysninger
  MedlemsOversigt = [];
  ngOnInit(): void {
    // Henter rettighedsdefinationer
   this.restService.sendRetGetRequest().subscribe((rdata: any[]) => {
    this.rettighederOversigt = rdata;
    console.log('rettighederOversigt', this.rettighederOversigt);
    });

   // henter medlemsinformationer
   this.restService.sendMemGetRequest().subscribe((mdata: any[]) => {
    this.MedlemsOversigt = mdata;

    for (const navn of mdata){

      const fuldnavn = navn.Medlemsnummer +  ' ' + navn.Fornavn + ' ' + navn.Efternavn;
      navn.fuldnavn = fuldnavn;

    }
    console.log('MedlemsOversigt', this.MedlemsOversigt);
    // Temp til valg af medlem
    this.mdropdownList = mdata;
    this.mdropdownSettings = {
    singleSelection: true,
    idField: 'members_id',
    textField: 'fuldnavn',
    itemsShowLimit: 3,
    enableCheckAll: false,
    allowSearchFilter: true

  };
    });
  }

  onmItemSelect(item: any) {
    this.restService.sendMemberByIdRequest(item).subscribe((mIddata: any[]) => {
      this.MemberById = [mIddata];
      console.log('memberById', this.MemberById);
      });
    }
}
