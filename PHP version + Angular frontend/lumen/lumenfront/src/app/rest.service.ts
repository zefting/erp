import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private httpClient: HttpClient) { }
  private RetAPi = 'http://localhost:8000/api/rettigheders';
  private MemberAPi = 'http://localhost:8000/api/members';

  public sendRetGetRequest(){
    return this.httpClient.get(this.RetAPi);
  }

  public sendMemGetRequest(){
    return this.httpClient.get(this.MemberAPi);
  }

  public sendMemberByIdRequest(item){
    return this.httpClient.get(this.MemberAPi + 'getMemberById/' + item.memberId);
  }

  public putMemberChangeRequest(item){
    const updatedata = {

    };
    return this.httpClient.put(this.MemberAPi + item.memberId, updatedata)
    .subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    );
  }

}
