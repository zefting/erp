import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StamkortComponent } from './stamkort/stamkort.component';

const routes: Routes = [

  { path: '',  redirectTo: '/stamkort', pathMatch: 'full' },
  {path: 'stamkort' , component: StamkortComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
